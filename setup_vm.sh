#!/bin/sh
while [ ! -f /tmp/cloud-init-finished ]
do
  sleep 5 
  tail -5  /var/run/hardening.log
done
sudo rm -rf /tmp/cloud-init-finished

#cloud-config

cloud_init_modules:
 - migrator
 - seed_random
 - bootcmd
 - write-files
# - growpart
# - resizefs
 - disk_setup
 - mounts
 - set_hostname
 - update_hostname
 - update_etc_hosts
 - ca-certs
 - rsyslog
 - users-groups
 - ssh

## Configure partions
runcmd:
  - printf 'Fix\n1\nyes\n20GB\n' | parted ---pretend-input-tty /dev/vda resizepart
  - resize2fs /dev/vda1
  - parted -s /dev/vda mkpart DATA ext4 20GB 30GB
  - parted -s /dev/vda mkpart DATA ext4 30GB 40GB
  - parted -s /dev/vda mkpart DATA ext4 40GB 80GB
  - parted -s /dev/vda mkpart DATA ext4 80GB 84GB
  - parted -s /dev/vda mkpart DATA ext4 84GB 89GB
  - parted -s /dev/vda mkpart DATA ext4 89GB 90GB
  - parted -s /dev/vda mkpart DATA ext4 90GB 91GB
  - parted -s /dev/vda mkpart DATA ext4 91GB 92GB
  - sudo partprobe /dev/vda
  - mkfs.xfs /dev/vda2
  - mkfs.xfs /dev/vda3
  - mkfs.xfs /dev/vda4
  - mkfs.xfs /dev/vda5
  - mkfs.xfs /dev/vda6
  - mkfs.xfs /dev/vda7
  - mkfs.xfs /dev/vda8
  - mkfs.xfs /dev/vda9
  - mkdir -p /run/temp
  - mount /dev/vda2 /run/temp
  - rsync -aAX /var/ /run/temp
  - umount /run/temp
  - mount /dev/vda2 /var
  - echo "/dev/vda2 /var auto defaults,noatime" >> /etc/fstab
  - mkdir -p /var/lib/kubelet
  - mount /dev/vda3 /var/lib/kubelet -o auto,defaults,noatime
  - echo "/dev/vda3 /var/lib/kubelet auto defaults,noatime" >> /etc/fstab
  - mkdir -p /var/lib/rancher/rke2/agent/containerd
  - mount /dev/vda4 /var/lib/rancher/rke2/agent/containerd -o "auto,defaults,noatime"
  - echo "/dev/vda4 /var/lib/rancher/rke2/agent/containerd auto defaults,noatime" >> /etc/fstab
  - mkdir -p /var/lib/rancher/rke2/server/db
  - mount /dev/vda5 /var/lib/rancher/rke2/server/db -o "auto,defaults,noatime"
  - echo "/dev/vda5 /var/lib/rancher/rke2/server/db auto defaults,noatime" >> /etc/fstab
  - mount /dev/vda6 /run/temp
  - rsync -aAX /var/log/ /run/temp
  - umount /run/temp
  - mount /dev/vda6 /var/log -o "auto,defaults,noatime"
  - echo "/dev/vda6 /var/log auto defaults,noatime" >> /etc/fstab
  - mkdir -p /var/log/audit
  - mount /dev/vda7 /var/log/audit -o "auto,defaults,noatime"
  - echo "/dev/vda7 /var/log/audit auto defaults,noatime" >> /etc/fstab
  - mkdir -p /var/tmp
  - mount /dev/vda8 /run/temp
  - rsync -aAX /var/tmp/ /run/temp
  - umount /run/temp
  - mount /dev/vda8 /var/tmp
  - echo "/dev/vda8 /var/tmp auto defaults,noatime" >> /etc/fstab
  - mount /dev/vda9 /run/temp
  - rsync -aAX /home /run/temp
  - umount /run/temp
  - mount /dev/vda9 /home -o "auto,defaults,noatime"
  
## Configure proxy, UPDATE with your values
  - export PROXY=http://123.123.123.123:3128
  - echo "Acquire::http::Proxy \"$PROXY\";" > /etc/apt/apt.conf.d/95-aptproxy
  - echo "Acquire::https::Proxy \"$PROXY\";" >> /etc/apt/apt.conf.d/95-aptproxy
  - export http_proxy=$PROXY
  - export https_proxy=$PROXY
  - export ftp_proxy=$PROXY
  - export no_proxy=127.0.0.1,localhost

## Update all packages and install the required ones
  - apt update && apt upgrade -y > /var/run/hardening.log
  - apt install -y ansible git wget >> /var/run/hardening.log

## Configure sshd to accept our key
  - echo "PubkeyAcceptedKeyTypes=+ssh-rsa" >> /etc/ssh/sshd_config
  - systemctl restart sshd

  - export INSTALL_RKE2_VERSION=v1.24.8+rke2r1
  - curl -sfL https://get.rke2.io | bash -

## Uncomment and UPDATE the rke2 version if you need to Install RKE2 and download images airgapped
#  - mkdir -p /var/lib/rancher/rke2/agent/images/
#  - wget https://github.com/rancher/rke2/releases/download/$INSTALL_RKE2_VERSION/rke2-images-core.linux-amd64.tar.zst -O /var/lib/rancher/rke2/agent/images/rke2-images-core.linux-amd64.tar.zst
#  - wget https://github.com/rancher/rke2/releases/download/$INSTALL_RKE2_VERSION/rke2-images-cilium.linux-amd64.tar.zst -O /var/lib/rancher/rke2/agent/images/rke2-images-cilium.linux-amd64.tar.zst
#  - wget https://github.com/rancher/rke2/releases/download/$INSTALL_RKE2_VERSION/rke2-images-calico.linux-amd64.tar.zst -O /var/lib/rancher/rke2/agent/images/rke2-images-calico.linux-amd64.tar.zst

## Configure ufw rules
  - chmod +x /tmp/fw_rules.sh && bash /tmp/fw_rules.sh >> /var/run/hardening.log

## Deploy ansible-lockdown
  - ansible-galaxy install -r /tmp/requirements.yml  >> /var/run/hardening.log
  - ansible-playbook /tmp/site.yml --skip-tags="rule_3.5.3.2.3,rule_4.2.1.5"  >> /var/run/hardening.log

## Run additional hardening scripts
  - chmod +x /tmp/hardening.sh
  - bash /tmp/hardening.sh  >> /var/run/hardening.log

## Remove dummy root password
  - sed -i 's/Password1//g' /etc/shadow

## Configure system
  - echo -e '\033[1;32m' "Configuring OS hardening...\e[0m"
  - echo "net.ipv4.conf.all.accept_source_route=0" >> /etc/sysctl.conf
  - echo "net.ipv4.conf.default.accept_source_route=0" >> /etc/sysctl.conf
  - echo "net.ipv4.conf.all.rp_filter=0" >> /etc/sysctl.conf
  - echo "net.ipv4.conf.default.rp_filter=0" >> /etc/sysctl.conf
  - echo "net.ipv6.conf.all.accept_source_route=0" >> /etc/sysctl.conf
  - echo "net.ipv6.conf.default.accept_source_route=0" >> /etc/sysctl.conf
  - sed -i '/net.ipv4.conf.all.rp_filter=1/d' /etc/sysctl.conf
  - sed -i '/net.ipv4.conf.default.rp_filter=1/d' /etc/sysctl.conf
  - sysctl --system
  - aa-enforce /etc/apparmor.d/*

## Fix UBUNTU20-CIS rule 1.3.1
  - aideinit --force --yes -b
  - touch /var/lib/aide/aide.db
  - chmod 600 /var/lib/aide/aide.db

## Force postfix to only listen on loopback interface
  - sed -i "s/^inet_interface.*/inet_interfaces = localhost/g" /etc/postfix/main.cf
  - sed -i "s/^UMASK.*/UMASK 027/g" /etc/login.defs

## Cleanup
  - apt remove --purge iputils-ping netcat wget tcpdump ansible perl gcc strace ltrace -y >> /var/run/hardening.log
  - apt autoremove -y >> /var/run/hardening.log
  - rm -rf /tmp/requirements.yml /tmp/site.yml /tmp/hardening.sh /etc/apt/apt.conf.d/95-aptproxy ~/.ansible
  - touch /tmp/cloud-init-finished
  
## Uncomment if you need debugging before new image is created
# sleep 3600


# Packer Cis Hardening

## Scope

The scope of these scripts is to create a hardened Ubuntu image according to CIS standards.

## Prerequisites

In order to use the scripts we will need to have Packer available. Installation instructions can be found [here](https://developer.hashicorp.com/packer/downloads).

## Usage

The first thing is to update the `openstack.json` file with your own openstack values. The variable names are pretty much self explanatory, with two observations:
- the `ssh_keypair_name` and `ssh_private_key_file` can be ommited, in which case Packer will create a temporary one and delete it when it finishes the job;
- the `source_image` must be the id of a clean Ubuntu 20.04 or 22.04 cloud image available in your tenant.

One other important thing is to update the proxy value in the `user_data.sh` file, around line 76.

You can adjust the firewall rules by editing the `fw_rules.sh` file if you have any special ports that you need to add to the permitted ones.

When finished editing the files, we can initialize Packer with the openstack builder

```
packer init
```

After the initialization is completed we can test and start the building process for our image:
```
packer validate
packer build openstack.json
```

The process takes some time (aprox 15min) as it will update all packages, run some ansible scripts to check/enforce the hardening rules and then cleans up everything. Keep in mind that the output is collected every 5s, but some things take longer without any output, so if you have the impression it froze, don't panic, just wait a few minutes and it should continue running.

After everything is finished, in the Packer output will be the new image id:
```
--> openstack: An image was created: d67a14d1-6d82-414d-8bad-e254f22a8d4f
```

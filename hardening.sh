#!/bin/bash
echo -e '\033[1;32m' "Updating all packages...\e[0m"
add-apt-repository -y ppa:deadsnakes/ppa
apt-get update && apt-cache search python3.1
echo -e '\033[1;32m' "Installing python3.11\e[0m"
apt-get install python3.11 -y
ln -sf /usr/bin/python3.11 /usr/bin/python
echo -e '\033[1;32m' "Installing pip and upgrading modules...\e[0m"
apt install pip -y
sudo rm -rf /usr/local/lib/python3.8/dist-packages/OpenSSL
sudo rm -rf /usr/local/lib/python3.8/dist-packages/pyOpenSSL-22.1.0.dist-info/
pip3 install pyOpenSSL==22.1
pip3 list --outdated --format=freeze | grep -v '^\-e' | cut -d = -f 1 | xargs -n1 pip3 install -U --ignore-installed
python_openssl_crypto_file="/usr/lib/python3/dist-packages/OpenSSL/crypto.py"
search_term="CB_ISSUER_CHECK = _lib.X509_V_FLAG_CB_ISSUER_CHECK"
cb_issuer_check_line_number="$(awk "/$search_term/ {print FNR}" $python_openssl_crypto_file)"
sed -i "${cb_issuer_check_line_number}s/.*/    # $search_term/" $python_openssl_crypto_file
pip3 list --outdated --format=freeze | grep -v '^\-e' | cut -d = -f 1 | xargs -n1 pip3 install -U --ignore-installed
sudo rm -rf /usr/local/lib/python3.8/dist-packages/OpenSSL
sudo rm -rf /usr/local/lib/python3.8/dist-packages/pyOpenSSL-22.1.0.dist-info/
pip3 list --outdated  | grep -v '^\-e' | cut -d = -f 1 | xargs -n1 pip3 install -U --ignore-installed
echo -e '\033[1;32m' "Removing unnecessary packages...\e[0m"
apt autoremove -y
pip uninstall ansible-core -y
echo -e '\033[1;32m' "Ensure sticky bit is set on all world-writable directories\e[0m"
df --local -P | awk '{if (NR!=1) print $6}' | xargs -I '{}' find '{}' -xdev -type d \( -perm -0002 -a ! -perm -1000 \) 2>/dev/null | xargs -I '{}' chmod a+t '{}'
sed -i "s/^UMASK.*/UMASK 027/g" /etc/login.defs
echo "session optional  pam_mkhomedir.so  umask=0027" |sudo tee -a  /etc/pam.d/common-session
sed -i "s/^inet_interface.*/inet_interfaces = localhost/g" /etc/postfix/main.cf
systemctl restart postfix
echo -e '\033[1;32m' "Done\e[0m"
